﻿namespace SevSharks.SignalR.Contracts
{
    /// <summary>
    /// Тип уведомления SignalR
    /// </summary>
    public enum SignalRNotificationType
    {
        LotHasBeenPublished = 5,

        ErrorDuringLotPublish = 10,

        LotStateHasBeenChanged = 15,

        ErrorDuringLotStateChange = 20,

        ApplicationHasBeenPublished = 25,

        ErrorDuringApplicationPublish = 30,

        ApplicationStateHasBeenChanged = 35,

        ErrorDuringApplicationStateChange = 40, 

        BidHasBeenAdded = 45,

        ErrorDuringAddingBid = 50,

        ProtocolHasBeenCreated = 55,

        LotsGridHasBeenUpdated = 60,

        LotHasBeenEdited = 65,

        ErrorDuringLotEdit = 70,

        TradeHasBeenEdited = 75,

        ErrorDuringTradeEdit = 80,
    }
}
