﻿namespace SevSharks.SignalR.Contracts
{
    /// <summary>
    /// Имена очередей для шины
    /// </summary>
    public static class Queues
    {
        /// <summary>
        /// Имя очереди для SignalR
        /// </summary>
        public const string SignalRQueue = "IT2.SignalR";
        
        /// <summary>
        /// Имя очереди для SignalR всем пользователям
        /// </summary>
        public const string SignalRSendToAll = "IT2.SignalRSendToAll";
    }
}
