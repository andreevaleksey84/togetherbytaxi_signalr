﻿using SolarLab.Common.Contracts;

namespace SevSharks.SignalR.Contracts
{
    /// <summary>
    /// Событие для SignalR всем пользователям
    /// </summary>
    public class SignalRSendToAllEvent : IWithQueueName
    {
        /// <summary>
        /// Имя очереди для события
        /// </summary>
        public string QueueName => Queues.SignalRSendToAll;

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Тип сообщения
        /// </summary>
        public SignalRNotificationType Type { get; set; }
    }
}
