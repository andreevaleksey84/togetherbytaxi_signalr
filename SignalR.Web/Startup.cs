﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SignalR.Web.Hubs;

namespace SignalR.Web
{
    public class Startup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// IConfiguration
        /// </summary>
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => options.AddPolicy("CorsPolicy",
                builder =>
                {
                    builder.AllowAnyHeader()
                           .AllowAnyMethod()
                           .SetIsOriginAllowed((host) => true)
                           .AllowCredentials();
                }));

            services.AddAuthorization();
            services.AddSingleton<IUserIdProvider, CustomUserIdProvider>();
            var identityServerAuthenticationAuthority = GetStringFromConfig("IdentityServerAuthentication", "Authority");
            var identityServerAuthenticationApiName = GetStringFromConfig("IdentityServerAuthentication", "ApiName");
            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = identityServerAuthenticationAuthority;
                    options.ApiName = identityServerAuthenticationApiName;
                    options.TokenRetriever = CustomTokenRetriever.FromHeaderAndQueryString;
                    options.RequireHttpsMetadata = false;
                });

            services
                .InstallConfiguration(Configuration)
                .ConfigureBusManager()
                .ConfigureAutomapper()
                .AddSignalR();

            string GetStringFromConfig(string firstName, string secondName)
            {
                var result = Configuration[$"{firstName}:{secondName}"];
                if (string.IsNullOrEmpty(result))
                {
                    result = Configuration[$"{firstName}_{secondName}"];
                }

                if (string.IsNullOrEmpty(result))
                {
                    throw new Exception($"Configuration setting does not exist. Setting name {firstName}:{secondName}");
                }

                return result;
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseAuthentication();

            app.UseHttpsRedirection();
            //app.UseStaticFiles();
            //app.UseCookiePolicy();

            app.UseCors("CorsPolicy");
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<UserHub>("/userhub");
                endpoints.MapHub<MessageHub>("/messagehub");
            });
        }
    }
}
