﻿using Microsoft.AspNetCore.SignalR;
using SignalR.Web.Models;
using System.Threading.Tasks;

namespace SignalR.Web.Hubs
{
    public class MessageHub : Hub
    {
        public async Task NewMessage(Message msg)
        {
            await Clients.Others.SendAsync("MessageReceived", msg);
        }

        public async Task DirectMessage(DirectMessage msg)
        {
            await Clients.Others.SendAsync("DirectMessageReceived", msg);
        }
    }
}
