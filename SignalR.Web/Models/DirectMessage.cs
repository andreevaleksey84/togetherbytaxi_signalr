﻿using System;

namespace SignalR.Web.Models
{
    public class DirectMessage
    {
        public string clientidfrom { get; set; }
        public string clientidto { get; set; }
        public string type { get; set; }
        public string message { get; set; }
        public DateTime date { get; set; }
    }
}
