﻿using System;
using SolarLab.BusManager.Abstraction;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Formatting.Json;

namespace SignalR.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.WithProperty("ApplicationName", "SevSharks.Signalr")
                .WriteTo.Console(new JsonFormatter())
                .CreateLogger();

            var host = CreateWebHostBuilder(args)
                .Build();

            var services = host.Services;
            var logger = services.GetRequiredService<ILogger<Program>>();
            logger.LogInformation("Signalr starting");
            try
            {
                logger.LogInformation("Connect to bus");
                var busManager = services.GetService<IBusManager>();
                busManager.StartBus(ServiceBusConfigurator.GetBusConfigurations(services));
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error during start bus for Signalr");
            }
            logger.LogInformation("Bus connection done");

            logger.LogInformation("Service RTSTender.IT2.SignalR started");
            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost
                .CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddSerilog();
                })
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                    config.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true);
                    config.AddJsonFile("secrets/appsettings.secrets.json", optional: true);
                    config.AddEnvironmentVariables();
                    config.AddCommandLine(args);
                })
                .UseStartup<Startup>()
                .UseSerilog();
    }
}
